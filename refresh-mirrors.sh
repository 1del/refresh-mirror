#!/usr/bin/env sh

echo "Ranking ArchLinux Mirrors..."
echo ""
pkexec /usr/bin/rate-mirrors --protocol=https --concurrency=16 --per-mirror-timeout=3000 --allow-root --save=/etc/pacman.d/mirrorlist arch
echo ""
echo ""

read -n 1 -s -r -t 10 -p "Press any key to exit..."; exit 0
